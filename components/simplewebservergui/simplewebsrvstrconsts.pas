unit SimpleWebSrvStrConsts;

{$mode objfpc}{$H+}

interface

resourcestring
  rsSWSTitle = 'Simple Web Server';
  rsSWSPathOfCompileserver = 'Path of compileserver';
  rsSWSAddress = 'Address';
  rsSWSPort = 'Port';
  rsSWSBindAny = 'Bind Any';
  rsSWSUserOrigin = 'User';

implementation

end.

